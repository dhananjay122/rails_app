# Rails-app


# Move to root directory
cd rails_app
#
![alt text](<Screenshot (28).png>)

# Docker compose build will build containers from docker compose yml
docker compose build 
# Do check the image
docker ps -a
#
![alt text](<Screenshot (39).png>)



# Compose up is like octopus having multiple hands to run multiple containers at a time
docker compose up
#
![alt text](<Screenshot (40).png>)


# To see the page you have two ways 

1.go to docker desktop and click on 8080:3000
#
2.type localhost:8080 on web browser 
#
![alt text](<Screenshot (31).png>)
#
![alt text](<Screenshot (32).png>)
#
![alt text](<Screenshot (33)-1.png>)
#
![alt text](<Screenshot (34).png>)

# You will get rails app main page
# To stop press ctrl+C

# ERRORS.pdf contains errors faced during 
It's there in root directory of the rails_app
