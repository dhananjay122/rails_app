 #Dockerfile

# Use an official Ruby runtime as a parent image
FROM ruby:2.7.4
RUN mkdir -p /app
# Set the working directory to /app
WORKDIR /app
COPY package.json .*/
# Copy the current directory contents into the container at /app
COPY . /app
COPY Gemfile ./
ADD Gemfile Gemfile.lock /app/
COPY Gemfile Gemfile.lock ./

RUN gem update system 

RUN apt-get update -qq &&\
    apt-get install -y python3 &&\
    apt-get install sqlite3 && \
    apt-get install -y nodejs npm &&\
    npm install -g yarn 
    
 
ENV MYSQL_DATABASE=rec_2024
ENV MYSQL_USER=vedant
ENV MYSQL_PASSWORD=Infinity@1


RUN gem update --system 3.2.3

RUN gem install rails

RUN gem install bundler -v 2.4.22

RUN gem install sqlite3 -v 1.4

RUN bundle update

RUN gem install execjs

COPY package.json yarn.lock ./
RUN rails webpacker:install

 
RUN bundle install
RUN bundle clean --force
COPY . .
ADD . /app
# Make port 3000 available to the world outside this container
EXPOSE 3000

# Start the Rails application
CMD ["rails", "server", "-b", "0.0.0.0"]









